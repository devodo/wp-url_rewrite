DB_HOST='localhost'
DB_PREFIX='wp'

# Define some basic usage to return back to the user if there is a parameter missing
usage() { echo -e "
Usage: $0 [-c <Old URL>] $1 [-n <New URL>] $2
Example: ${GREEN}$0 -c http://example.com${RESET} -n http://example-new.com -p \"PASSWORD\"" 1>&2; exit 1; }

# Read in parameters
while getopts "p:f:d:u:c:n:h:" o; do
    case "${o}" in
        c)
            c=${OPTARG}
            ;;
        n)
            n=${OPTARG}
            ;;
        p)
            DB_PASS=${OPTARG}
            ;;
        f)
            DB_PREFIX=${OPTARG}
            ;;
        u)
            DB_USER=${OPTARG}
            ;;
        d)
            DB_NAME=${OPTARG}
            ;;
        h)
            DB_HOST=${OPTARG}
            ;;
        *)
            usage
            ;;
    esac
done
shift $((OPTIND-1))

# If no parameter specified then show usage.
if [ -z "${c}" ] || [ -z "${n}" ]; then
    usage
fi

while ! mysql -h ${DB_HOST} -u ${DB_USER} -p${DB_PASS} -e ";" ; do
  echo "Cannot connect to MySQL server"
  exit 0
done

#  Re-write the core urls
echo "Rewriting _options table"
mysql -h ${DB_HOST} -u ${DB_USER} -p${DB_PASS} ${DB_NAME} -e "UPDATE ${DB_PREFIX}_options SET option_value = replace(option_value, '${c}', '${n}') WHERE (option_name = 'home' OR option_name = 'siteurl' OR option_name = 'fileupload_url')"
echo "Done!"

# Re-write the posts table
echo "Rewriting _posts table"
mysql -h ${DB_HOST} -u ${DB_USER} -p${DB_PASS} ${DB_NAME} -e "UPDATE ${DB_PREFIX}_posts SET guid = replace(guid, '${c}', '${n}')"
echo "Done!"

# Re-write the post_content field table
echo "Rewriting _posts post_content fields"
mysql -h ${DB_HOST} -u ${DB_USER} -p${DB_PASS} ${DB_NAME} -e "UPDATE ${DB_PREFIX}_posts SET post_content = replace(post_content, '${c}', '${n}')"
echo "Done!"

# Re-write the postmeta table
echo "Rewriting _postmeta meta_value fields=meta_value"
mysql -h ${DB_HOST} -u ${DB_USER} -p${DB_PASS} ${DB_NAME} -e "UPDATE ${DB_PREFIX}_postmeta SET meta_value = replace(meta_value, '${c}', '${n}')"
echo "Done!"

# Re-write the _sitemeta table
if [ $(mysql -N -s -u${DB_USER} -p${DB_PASS} -e \
    "SELECT COUNT(*) FROM information_schema.tables WHERE \
        table_schema='${DB_NAME}' AND table_name='${DB_PREFIX}_sitemeta';") -eq 1 ]; then
    echo "Rewriting _sitemeta table field=meta_value"
    mysql -h ${DB_HOST} -u ${DB_USER} -p${DB_PASS} ${DB_NAME} -e "UPDATE ${DB_PREFIX}_sitemeta SET meta_value = replace(meta_value, '${c}', '${n}') WHERE meta_key='source_domain'"
    echo "Done!"
else
    echo "Table ${DB_PREFIX}_sitemeta does not exist"
fi

# Re-write the final wp_usermeta table
echo "Rewriting _usermeta table field=meta_value"
mysql -h ${DB_HOST} -u ${DB_USER} -p${DB_PASS} ${DB_NAME} -e "UPDATE wp_usermeta SET meta_value='${n}' WHERE meta_key='source_domain'"
echo "Done!"
